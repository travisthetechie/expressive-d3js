"use strict";

var assert = require('assert');
var _ = require('lodash');
var statsExtractor = require('../services/statsExtractor');

describe('statsExtractor', function(){
    describe('getOperationCounts', function(){
        it('should return accurate event counts', function(){
            var events = [{
                webhookEvent: "event_1"
            }, {
                webhookEvent: "event_1"
            }, {
                webhookEvent: "event_2"
            }];

            var expectedResult = {
                "event_1": 2,
                "event_2": 1
            };

            assert.deepEqual(expectedResult, statsExtractor.getOperationCounts(events));
        });
    });

    describe('getOperationTypeUsage', function(){
        it('should return accurate event usage percentages', function(){
            var events = [{
                webhookEvent: "event_1"
            }, {
                webhookEvent: "event_1"
            }, {
                webhookEvent: "event_2"
            }, {
                webhookEvent: "event_3"
            }, {
                webhookEvent: "event_3"
            }, {
                webhookEvent: "event_3"
            }, {
                webhookEvent: "event_3"
            }, {
                webhookEvent: "event_3"
            }];

            var expectedResult = {
                "event_1": 25,
                "event_2": 12.5,
                "event_3": 62.5,
                "_total": 8
            };

            assert.deepEqual(expectedResult, statsExtractor.getOperationTypeUsage(events));
        });
    });

    describe('getOperationCountByUser', function(){
        it('should return accurate operations by user', function() {

            function createEvents(arr, userName, event, count) {
                for (var i = 0; i < count; i++) {
                    arr.push({
                        "webhookEvent": event,
                        "user": {
                            "name": userName
                        }
                    });
                }
            }

            var events = [];

            createEvents(events, "user1", "jira:issue_created", 10);
            createEvents(events, "user1", "jira:issue_updated", 10);
            createEvents(events, "user2", "jira:issue_created", 50);
            createEvents(events, "user2", "jira:issue_updated", 11);
            createEvents(events, "user3", "jira:issue_created", 15);
            createEvents(events, "user3", "jira:issue_updated", 12);
            createEvents(events, "user3", "jira:issue_deleted", 11);

            var expectedResult = {
                "jira:issue_created": {
                    "user2": 50,
                    "user3": 15,
                    "user1": 10
                },
                "jira:issue_updated": {
                    "user3": 12,
                    "user2": 11,
                    "user1": 10
                },
                "jira:issue_deleted": {
                    "user3": 11
                }
            };

            assert.deepEqual(expectedResult, statsExtractor.getOperationCountByUser(events));
        });
    });
});