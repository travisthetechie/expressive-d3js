"use strict";

var _ = require("lodash");
var SUPPORTED_OPERATIONS = ["jira:issue_created", "jira:issue_updated", "jira:issue_deleted"];

function getOperationCountBy(events, predicate) {
    var results = {};

    _.each(events, function(event) {
        var webhookEvent = event.webhookEvent;
        if (_.contains(SUPPORTED_OPERATIONS, webhookEvent)) {
            if (!results[webhookEvent]) {
                results[webhookEvent] = {};
            }

            var property = predicate(event);
            if (!results[webhookEvent][property]) {
                results[webhookEvent][property] = 1;
            } else {
                results[webhookEvent][property]++;
            }
        }
    });

    return results;
}

module.exports = {

    getOperationCounts: function(events) {
        var operations = {};

        events.forEach(function(event) {
            if (!operations[event.webhookEvent]) {
                operations[event.webhookEvent] = 1;
            } else {
                operations[event.webhookEvent]++;
            }
        });

        return operations;
    },

    getOperationTypeUsage: function(events) {
        var total = events.length;
        var percentages = {
            "_total": total
        };
        var counts = this.getOperationCounts(events);

        _.keys(counts).forEach(function(key) {
            percentages[key] = counts[key] / total * 100;
        });

        return percentages;
    },

    getOperationCountByUser: function(events) {
        return getOperationCountBy(events, function(event) {
            return event.user.name;
        });
    },

    getOperationCountByProject: function(events) {
        return getOperationCountBy(events, function (event) {
            return event.issue.fields.project.name;
        });
    }

};