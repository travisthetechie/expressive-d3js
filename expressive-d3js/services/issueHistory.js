"use strict";

var datastore = require("./datastore");
var _ = require("lodash");

// Important to note that you should not include the context path here.
var SEARCH_REST_ENDPOINT = "/rest/api/2/search";
var CHANGABLE_FIELDS = [
    "assignee",
    "status",
    "fixVersion",
    "priority",
    "reporter",
    "resolution"
];
var JQL_HISTORY_QUERY = " changed AFTER -1w or ";
var UPDATED_QUERY = SEARCH_REST_ENDPOINT + "?expand=changelog&jql=" + CHANGABLE_FIELDS.join(JQL_HISTORY_QUERY) + " changed AFTER -1w";
var CREATED_QUERY = SEARCH_REST_ENDPOINT + "?jql=createdDate > -1w";

function transformChangelog(issues) {

    var transformed = [];

    issues.forEach(function(issue) {
        issue.changelog.histories.forEach(function(history) {
            var t = {
                webhookEvent: "jira:issue_updated",
                changelog: {
                    items: history.items
                },
                user: history.author,
                timestamp: new Date(history.created).getTime()
            };

            delete issue.changelog;

            t.issue = issue;

            transformed.push(t);
        });
    });

    return transformed;
}

function transformCreatelog(issues) {

    var created = [];

    if (issues && issues.length) {
        created = issues.map(function(issue) {
            return {
                webhookEvent: "jira:issue_created",
                user: issue.fields.creator,
                issue: issue,
                timestamp: new Date(issue.fields.created).getTime()
            }
        });
    }

    return created;
}

module.exports = function(httpClient) {
    httpClient.get({
        url: UPDATED_QUERY,
        json: true
    }, function(err, response, updatedIssues) {
        if (response.statusCode === 200) {
            var changelog = transformChangelog(updatedIssues.issues);

            httpClient.get({
                url: CREATED_QUERY,
                json: true
            }, function(err, response, createdIssues) {

                changelog.push.apply(changelog, transformCreatelog(createdIssues.issues));

                var lastWeek = new Date().getTime() - 604800000;
                if (changelog.length) {
                    datastore.remove({}, { multi: true }, function() {
                        datastore.insert(changelog, function() {
                            console.log("Successfully updated last 7 days of events");
                        });
                    });
                }
            });
        }
    });
};