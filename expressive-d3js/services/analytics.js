"use strict";

var db = require('./datastore');
var statsExtractor = require('./statsExtractor');
var _ = require("lodash");

function get7DaysOfEvents(query, callback) {
    var lastWeek = new Date().getTime() - 604800000;
    db.find(_.extend({
        timestamp: {
            $gt: lastWeek
        }
    }, query || {}), callback);
}

function getStats(params, callback) {
    get7DaysOfEvents(params, function(err, events) {

        if (err) {
            return callback(err);
        }

        callback(null, {
            issueOperationTypes: statsExtractor.getOperationTypeUsage(events),
            issueOperationCounts: statsExtractor.getOperationCounts(events),
            operationsByUser: statsExtractor.getOperationCountByUser(events),
            operationsByProject: statsExtractor.getOperationCountByProject(events)
        });
    });

}

module.exports = {

    onEvent: function(event) {
        db.insert(event, function(err) {
            if (err) {
                console.error("Could not insert event document", err);
            }
        });
    },

    getInstanceStats: function(callback) {
        getStats({}, callback);
    },

    getProjectStats: function(projectId, callback) {
        getStats({
            "issue.fields.project.id": projectId
        }, callback);
    }

};
