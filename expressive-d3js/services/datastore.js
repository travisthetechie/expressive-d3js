"use strict";

var Datastore = require('nedb'),
    path = require('path'),
    dbPath = path.join(__dirname, "..", ".db", "geiga");

var db = new Datastore({
    filename: dbPath
});

db.loadDatabase(function(err) {
    if (err) {
        console.error("An error occurred loading the database", err);
    }
});

db.persistence.setAutocompactionInterval(10000);

module.exports = db;