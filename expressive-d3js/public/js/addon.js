(function() {

  var appendSelector = function(selector) {
    var self = this;
    var selectorParts = selector.split(/([\.\#])/);
    if (selectorParts.length == 0) return self;
    self = self.append(selectorParts.shift());

    while (selectorParts.length > 1) {
      var selectorModifier = selectorParts.shift();
      var selectorItem = selectorParts.shift();
      if (selectorModifier === ".") {
        self = self.classed(selectorItem, true);
      } else if (selectorModifier === "#") {
        self = self.attr('id', selectorItem);
      }
    }
    return self;
  };

  d3.selection.enter.prototype.appendSelector = appendSelector;
  d3.selection.prototype.appendSelector = appendSelector;

  // exporting for testing
  window.updateAction = function(action) {
    if (!action) return;

    var data = window.statsData || [];

    for(var index in data) {
      if (data[index].action == action) {
        data[index].value = data[index].value + 1;

        rescaleBarChart(window.barChart, data);

        return;
      }
    }

    data.push({
      action: action,
      value: 1
    });
    rescaleBarChart(window.barChart, data);
  };

  function getQueryParams(qs) {
    qs = qs.split("+").join(" ");

    var params = {}, tokens,
      re = /[?&]?([^=]+)=([^&]*)/g;

    while (tokens = re.exec(qs)) {
      params[decodeURIComponent(tokens[1])] =
        decodeURIComponent(tokens[2]);
    }

    return params;
  }

  apiCall = function(uri, callback) {
    AP.require('request', function(request) {
      request({
        url: uri,
        success: function(response) {
          callback(response);
        },
        error: function(response) {
          console.log("Error loading API (" + uri + ")");
          console.log(arguments);
        },
        contentType: "application/json"
      });
    });
  };

  window.AG = {};

  window.AG.initInstanceStats = function() {
    var params = getQueryParams(document.location.search);
    var baseUrl = params.xdm_e + params.cp + "/browse/";

    apiCall('/rest/api/2/project', function(response) {
      // convert the string response to JSON
      response = JSON.parse(response);

      var d = d3.select(".projects");

      var projTable = d.append('table')
        .classed({'project': true, 'aui': true});

      var projHeadRow = projTable.append("thead").append("tr");
      projHeadRow.append("th");
      projHeadRow.append("th").text("Key");
      projHeadRow.append("th").text("Name");

      var projBody = projTable.append("tbody");

      var row = projBody.selectAll("tr")
        .data(response)
        .enter()
        .append("tr");

      row.append("td").append('span')
        .classed({'aui-avatar': true, 'aui-avatar-xsmall': true})
        .append('span')
        .classed({'aui-avatar-inner': true})
        .append('img')
        .attr('src', function(item) { return item.avatarUrls["16x16"] });

      row.append("td").append('span')
        .classed({'project-key': true, 'aui-label': true})
        .text(function(item) { return item.key; });

      row.append("td").append('span')
        .classed({'project-name': true})
        .append("a")
        .attr('href', function(item) { return baseUrl + item.key; })
        // add "?selectedTab=atlas-geiger:project-stats" to the url?
        .attr('target', "_top")
        .text(function(item) { return item.name; });
    });

    $.getJSON('/api/stats', function(stats) {
      buildSimpleStatsBarChart(stats);
    });
  };

  var mapAction = function(webhookName) {

    var hooks = {
      "jira:issue_deleted": "Deletes",
      "jira:issue_created": "Creates",
      "jira:issue_updated": "Updates"
    };

    return hooks[webhookName] || webhookName;
  };

  var mapActionsToData = function(stats) {
    var data = [];

    for(var prop in stats.issueOperationCounts) {
      data.push({
        action: mapAction(prop),
        value: stats.issueOperationCounts[prop]
      });
    }

    window.statsData = data;

    return data;
  };

  var rescaleBarChart = function(chart, data) {
    var x = d3.scale.linear()
      .domain([0, d3.sum(data, function(d) { return d.value; })])
      .range([0, 100]);

    chart.data(data, function(d) { return d.action; });

    chart
      .style("width", function(d) { return x(d.value) + "%"; });
  };

  var buildSimpleStatsBarChart = function(stats) {
    var data = mapActionsToData(stats);

    var chart = d3.select(".charts")
      .selectAll("div")
      .data(data, function(d) { return d.action; });

    window.barChart = chart;

    chart
      .enter()
      .append("div")
      .classed({ "bar-item": true })
      .append("span");

    chart
      .exit()
      .remove();

    chart
      .selectAll("span")
      .text(function(d) { return d.action; });

    rescaleBarChart(chart, data);
  };

  var websocket = new WebSocket("ws://localhost:3030/");
  websocket.onmessage = function(evt) {
    console.log(evt.data);
    if (window.barChart && window.statsData)
      updateAction(mapAction(evt.data));
  };
})();
