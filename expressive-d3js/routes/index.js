var analytics = require('../services/analytics');
var issueHistory = require('../services/issueHistory');

module.exports = function (app, addon) {

  var WebSocketServer = require('ws').Server;
  var wss = new WebSocketServer({port: 3030});
  wss.on('connection', function(ws) {
    ws.send('connected');
  });

  wss.broadcast = function(data) {
    for(var i in this.clients)
      this.clients[i].send(data);
  };

  // Root route. This route will serve the `atlassian-connect.json` unless the
  // documentation url inside `atlassian-connect.json` is set
  app.get('/', function (req, res) {
    res.format({
      // If the request content-type is text-html, it will decide which to serve up
      'text/html': function () {
        res.redirect('/atlassian-connect.json');
      },
      // This logic is here to make sure that the `atlassian-connect.json` is always
      // served up when requested by the host
      'application/json': function () {
        res.redirect('/atlassian-connect.json');
      }
    });
  });

  // This is an example route that's used by the default "generalPage" module.
  // Verify that the incoming request is authenticated with Atlassian Connect
  app.get('/hello-world', addon.authenticate(), function (req, res) {
      // Rendering a template is easy; the `render()` method takes two params: name of template
      // and a json object to pass the context in
      res.render('hello-world', {
        title: 'Atlassian Connect'
        //issueId: req.query('issueId')
      });
    }
  );

  // Add any additional route handlers you need for views or REST resources here...
  app.get('/stats/instance', addon.authenticate(), function (req, res) {
    res.render('stats-instance', {
      title: "Instance stats"
    });
  });

  // Add any additional route handlers you need for views or REST resources here...
  var events = [
    '/issues/created',
    '/issues/deleted',
    '/issues/updated'
  ];

  events.map(function (event) {
    app.post(event, addon.authenticate(), function (req, res) {
      analytics.onEvent(req.body);
      wss.broadcast(req.body.webhookEvent);
      res.send(204);
    });
  });

  app.post("/addon/enabled", function (req, res) {
    issueHistory(addon.httpClient(req.body));
  });

  var dumpJson = function (res, err, data) {
    res.set('Content-Type', 'application/json');

    if (err) {
      return res.send(500, { error: err });
    }

    res.json(data);
  };

  app.get('/api/stats', function (req, res) {
    analytics.getInstanceStats(function (err, data) {
      dumpJson(res, err, data);
    });
  });
};
