# Getting Started

1. Install vagrant
2. `vagrant up`
3. `vagrant ssh` and `atlas-run-connect --product jira` once you are in the vm
4. Wait until you see `[INFO] jira started successfully in 90s at http://localhost:2990/jira` and 
   visit [http://localhost:2990/jira]
5. New console, `vagrant ssh` and `cd /vagrant/expressive-d3js && npm install && node app`
6. Create some projects, some issues, and then hit "Stats" in the header to see the page.
   This demo uses websockets for updates, so once you have a bar chart appearing on that page feel free to open up a
   new window and watch the bar adjust in real time. 